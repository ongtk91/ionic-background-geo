import { Component, ViewChild, ElementRef } from '@angular/core';

import { GoogleMaps, GoogleMap, CameraPosition, LatLng, GoogleMapsEvent, Marker, MarkerOptions } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

declare var google;

@Component({
  selector: 'page-gmaphome',
  templateUrl: 'gmaphome.html'
})
export class GMapHomePage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  map: GoogleMap;

  constructor(private googleMaps: GoogleMaps, private geoLocation: Geolocation, private alertCtrl: AlertController) {
    
  }
  
  ngAfterViewInit() {
    let loc: LatLng;

    this.initMap();

    this.getLocation().then( res => {
      console.log(res.coords.latitude);
      console.log(res.coords.longitude);

      loc = new LatLng(res.coords.latitude, res.coords.longitude);
      this.moveCamera(loc);

      this.createMarker(loc, "HereYouAre!").then( (marker: Marker) => {
        marker.showInfoWindow();
      }).catch(err => {
        console.log(err);
        let errorAlert = this.alertCtrl.create({
          title: 'Ops',
          subTitle: 'Please turn on your location and go outdoor! [' + err.toString() + ']',
          buttons: ['OK']
        });
        errorAlert.present();
      });
    }).catch( err => {
      console.log(err);
      let errorAlert = this.alertCtrl.create({
        title: 'Ops',
        subTitle: 'Please turn on your location and go outdoor! [' + err.toString() + ']',
        buttons: ['OK']
      });
      errorAlert.present();
    });

    this.startNavigating();
  }
  
  initMap() {
    let element = this.mapElement.nativeElement;
    this.map = this.googleMaps.create(element);
  }

  getLocation() {
    return this.geoLocation.getCurrentPosition();
  }

  moveCamera(loc: LatLng) {
    let options: CameraPosition<any> = {
      target: loc,
      zoom: 15,
      tilt: 10
    }
    this.map.moveCamera(options);
  }

  createMarker(loc: LatLng, title: string) {
    let marketOptions: MarkerOptions = {
      position: loc,
      title: title
    };

    return this.map.addMarker(marketOptions);
  }

  selfTrack() {
    let loc: LatLng;
    
    this.getLocation().then( res => {
      console.log(res.coords.latitude);
      console.log(res.coords.longitude);

      loc = new LatLng(res.coords.latitude, res.coords.longitude);
      this.moveCamera(loc);

      this.createMarker(loc, "HereYouAre!").then( (marker: Marker) => {
        marker.showInfoWindow();
      }).catch(err => {
        console.log(err);
        let errorAlert = this.alertCtrl.create({
          title: 'Ops',
          subTitle: 'Please turn on your location and go outdoor! [' + err.toString() + ']',
          buttons: ['OK']
        });
        errorAlert.present();
      });
    }).catch( err => {
      console.log(err);
      let errorAlert = this.alertCtrl.create({
        title: 'Ops',
        subTitle: 'Please turn on your location and go outdoor! [' + err.toString() + ']',
        buttons: ['OK']
      });
      errorAlert.present();
    });
  }

  startNavigating(){
           let directionsService = new google.maps.DirectionsService;
           let directionsDisplay = new google.maps.DirectionsRenderer;
    
           directionsDisplay.setMap(this.map);
           directionsDisplay.setPanel(this.directionsPanel.nativeElement);
    
           directionsService.route({
               origin: 'adelaide',
               destination: 'adelaide oval',
               travelMode: google.maps.TravelMode['DRIVING']
           }, (res, status) => {
    
               if(status == google.maps.DirectionsStatus.OK){
                   directionsDisplay.setDirections(res);
               } else {
                   console.warn(status);
               }
    
           });
    
  }
}
